<?php

/**
 * Plugin Name: WordPress PAB
 * Plugin URI: https://studio-creativa.pl
 * Description: A (unofficial) WordPress plugin to report PHP errors to PAB.
 * Version: 0.1.1
 * Author: Jacek Labudda
 * Author URI: https://studio-creativa.pl
 * License: MIT
 */

// Exit if accessed directly
defined('ABSPATH') || exit;

// If the plugin was already loaded as a mu-plugin or from somewhere else do not load again
if (defined('WP_PAB_MU_LOADED') || defined('WP_PAB_LOADED')) {
    return;
}

define('WP_PAB_LOADED', true);

define('WP_PAB_WPINC', ABSPATH . (defined('WPINC') ? WPINC : 'wp-includes'));
define('WP_PAB_WPADMIN', ABSPATH . 'wp-admin');

// Load the WordPress plugin API early so hooks can be used even if PAB is loaded before WordPress
if (!function_exists('add_action')) {
    require_once WP_PAB_WPINC . '/plugin.php';
}

// Make sure the PHP version is at least 7.2
if (!defined('PHP_VERSION_ID') || PHP_VERSION_ID < 70200) {
    function wp_pab_php_version_notice()
    { ?>
        <div class="error below-h2">
            <p>
                <?php printf(
                    'The WordPress PAB plugin requires at least PHP 7.2. You have %s. WordPress PAB will not be active unless this is resolved!',
                    PHP_VERSION
                ); ?>
            </p>
        </div>
    <?php }

    add_action('admin_notices', 'wp_pab_php_version_notice');

    return;
}

// Resolve the pab plugin file
define('WP_PAB_PLUGIN_FILE', call_user_func(static function () {
    global $wp_plugin_paths;

    $plugin_file = __FILE__;

    if (!empty($wp_plugin_paths)) {
        $wp_plugin_real_paths = array_flip($wp_plugin_paths);
        $plugin_path = wp_normalize_path(dirname($plugin_file));

        if (isset($wp_plugin_real_paths[$plugin_path])) {
            $plugin_file = str_replace($plugin_path, $wp_plugin_real_paths[$plugin_path], $plugin_file);
        }
    }

    return $plugin_file;
}));

// Load dependencies
if (!class_exists(WP_PAB_Version::class)) {
    $scopedAutoloader = __DIR__ . '/build/vendor/scoper-autoload.php';

    // If ther is a scoped autoloader we use that version, otherwise we use the normal autoloader
    require_once $scopedAutoloaderExists = file_exists($scopedAutoloader)
        ? $scopedAutoloader
        : __DIR__ . '/vendor/autoload.php';

    define('WP_PAB_SCOPED_AUTOLOADER', $scopedAutoloaderExists);
}

// Define the default version
if (!defined('WP_PAB_VERSION') && function_exists('wp_get_theme')) {
    define('WP_PAB_VERSION', wp_get_theme()->get('Version') ?: 'unknown');
}

// Load the PHP tracker if we have a PHP token
if (defined('WP_PAB_PHP_TOKEN') || defined('WP_PAB_TOKEN')) {
    $pab_php_tracker_token = defined('WP_PAB_TOKEN')
        ? WP_PAB_TOKEN
        : NULL;

    if (!empty($pab_php_tracker_token)) {
        WP_PAB_Tracker::get_instance();
    }
}

/**
 * Register a "safe" function to call PAB functions safer in your own code,
 * the callback only executed if a token was set and thus the client is able to sent events.
 *
 * Usage:
 * if ( function_exists( 'wp_pab_safe' ) ) {
 *     wp_pab_safe( function ( \PAB\State\HubInterface $client ) {
 *         $client->captureMessage( 'This is a test message!', \PAB\Severity::debug() );
 *     } );
 * }
 */
if (!function_exists('wp_pab_safe')) {
    /**
     * Call the callback with the PAB client, or not at all if there is no client.
     *
     * @param callable $callback
     */
    function wp_pab_safe(callable $callback)
    {
        if (class_exists('WP_PAB_Tracker')) {
            $tracker = WP_PAB_Tracker::get_instance();

            if (!empty($tracker->get_token())) {
                $callback($tracker->get_client());
            }
        }
    }
}


// Load the admin page
WP_PAB_Admin_Page::get_instance();

$pabToken = defined('WP_PAB_TOKEN') && (WP_PAB_TOKEN !== false);
$pabTracingRate = defined('WP_PAB_TRACES_SAMPLE_RATE') ? WP_PAB_TRACES_SAMPLE_RATE : false;
if ($pabToken && is_numeric($pabTracingRate) && $pabTracingRate != 0) {

    class Transaction
    {
        /**
         * @var \PAB\Tracing\TransactionContext
         */
        private $transactionContext;

        /**
         * @var string|null
         */
        private $url = null;

        /**
         * @var \PAB\Tracing\Span|null
         */
        private $appSpan = null;

        /**
         * @var \PAB\Tracing\Transaction|null
         */
        private $transaction = null;

        /**
         * @var float|null
         */
        private $bootedTimestamp;
        /**
         * @var \PAB\State\HubInterface
         */
        private $pabHub;

        public function __construct()
        {
            wp_pab_safe(function (\PAB\State\HubInterface $pabHub) {
                $this->pabHub = $pabHub;
                $rate = WP_PAB_TRACES_SAMPLE_RATE ?? 0;
                $pabHub->getClient()->getOptions()->setTracesSampleRate($rate);
                \PAB\PABSdk::setCurrentHub($pabHub);
            });

            $this->transactionContext = new \PAB\Tracing\TransactionContext();
            $this->bootedTimestamp = microtime(true);
        }

        public function init()
        {
            if ($_SERVER['REQUEST_URI'] ?? null) {
                $this->url = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                if (strpos($_SERVER['REQUEST_URI'], 'wp-admin/admin-ajax.php') !== false) {
                    $actionName = $_GET['action'] ?? $_POST['action'] ?? 'PAB_PARSE_ERROR_NO_ACTION';
                    /**
                     * If its admin ajax, the transaction name includes the name of the action instead of the URL.
                     */
                    $name = "admin-ajax: " . $actionName;
                    $this->transactionContext->setTags(['admin-ajax']);

                } else {
                    $name = $_SERVER['HTTP_HOST'] . parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
                    $this->transactionContext->setTags(['normal-request']);
                }
            } else {
                $this->url = "$_SERVER[HTTP_HOST]";
                if (defined('DOING_CRON')) {
                    $name = 'cron';
                    $this->transactionContext->setTags(['cron']);
                } else {
                    $commands = $_SERVER['argv'] ?? ['', 'PAB_PARSE_ERROR_NO_COMMAND'];
                    array_shift($commands); // the first one is the wp binary.
                    $command = implode(' ', $commands);
                    $name = 'wp ' . $command;
                    $this->transactionContext->setTags(['wp-cli']);
                }
            }
            $this->transactionContext->setName($name);
            $this->transactionContext->setOp('http.server');
            $this->transactionContext->setData([
                'url' => $this->url,
                'method' => strtoupper($_SERVER['REQUEST_METHOD'] ?? '')
            ]);
            $this->transactionContext->setStartTimestamp($_SERVER['REQUEST_TIME_FLOAT'] ?? microtime(true));

            $this->transaction = $this->pabHub->startTransaction($this->transactionContext);
            $this->pabHub->setSpan($this->transaction);

            $bootstrapSpan = $this->addAppBootstrapSpan();

            $spanContext = new \PAB\Tracing\SpanContext();
            $spanContext->setOp('wordpress.handle');
            $spanContext->setStartTimestamp($bootstrapSpan ? $bootstrapSpan->getEndTimestamp() : microtime(true));

            $this->appSpan = $this->transaction->startChild($spanContext);
            $this->pabHub->setSpan($this->appSpan);
        }

        public function complete()
        {
            if ($this->appSpan !== null) {
                $this->appSpan->finish();
            }
            $this->pabHub->setSpan($this->transaction);
            $this->transaction->finish();
        }

        private function addAppBootstrapSpan(): ?\PAB\Tracing\Span
        {
            if ($this->bootedTimestamp === null) {
                return null;
            }

            $spanContextStart = new \PAB\Tracing\SpanContext();
            $spanContextStart->setOp('wordpress.bootstrap');
            $spanContextStart->setStartTimestamp(microtime(true));
            $spanContextStart->setEndTimestamp($this->bootedTimestamp);

            $span = $this->transaction->startChild($spanContextStart);

            // Consume the booted timestamp, because we don't want to report the bootstrap span more than once
            $this->bootedTimestamp = null;

            // Add more information about the bootstrap section if possible
            $this->addBootDetailTimeSpans($span);

            return $span;
        }

        private function addBootDetailTimeSpans(\PAB\Tracing\Span $bootstrap): void
        {
            // This constant should be defined right after the composer `autoload.php` require statement in `public/index.php`
            // define('PAB_AUTOLOAD', microtime(true));
            if (!defined('PAB_AUTOLOAD') || !PAB_AUTOLOAD) {
                return;
            }

            $autoload = new SpanContext();
            $autoload->setOp('wordpress.autoload');
            $autoload->setStartTimestamp($bootstrap->getStartTimestamp());
            $autoload->setEndTimestamp(PAB_AUTOLOAD);

            $bootstrap->startChild($autoload);
        }
    }
    $transaction = new Transaction();
    add_action('shutdown', [$transaction, 'complete']);
    $transaction->init();
}

