<?php

final class WP_PAB_Admin_Page
{
    /**
     * @var self
     */
    private static $instance;

    public static function get_instance(): self
    {
        return self::$instance ?: self::$instance = new self;
    }

    protected function __construct()
    {
        add_action('init', function () {
            if (!is_admin()) {
                return;
            }

            add_action('admin_menu', [$this, 'admin_menu']);
            add_action('network_admin_menu', [$this, 'network_admin_menu']);
        });
    }

    public function admin_menu(): void
    {
        if (is_plugin_active_for_network(plugin_basename(WP_PAB_PLUGIN_FILE))) {
            return;
        }

        add_management_page(
            'WP PAB test',
            'WP PAB test',
            'activate_plugins',
            'pab-wp',
            [$this, 'render_admin_page']
        );
    }

    public function network_admin_menu(): void
    {
        if (!is_plugin_active_for_network(plugin_basename(WP_PAB_PLUGIN_FILE))) {
            return;
        }

        global $submenu;

        add_menu_page(
            '',
            'Tools',
            'activate_plugins',
            'pab-wp-tools-menu',
            '',
            'dashicons-admin-tools',
            22
        );

        add_submenu_page(
            'pab-wp-tools-menu',
            'WP PAB test',
            'WP PAB test',
            'activate_plugins',
            'pab-wp',
            [$this, 'render_admin_page']
        );

        if (!empty($submenu['pab-wp-tools-menu'][0]) && $submenu['pab-wp-tools-menu'][0][2] === 'pab-wp-tools-menu') {
            unset($submenu['pab-wp-tools-menu'][0]);
        }
    }

    private function send_test_event(): ?string
    {
        $exception = $this->generateTestException('wp PAB test', ['foo' => 'bar']);

        $tracker = WP_PAB_Tracker::get_instance();
        if (!empty($tracker->get_token())) {
            return $tracker->get_client()->captureException($exception);
        }

        return null;
    }

    private function generateTestException(string $command, array $arg): ?Exception
    {
        try {
            throw new Exception('This is a test exception sent from the PAB WP PHP integration.');
        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function render_admin_page(): void
    {
        $test_event_sent = false;
        $test_event_id = null;

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $test_event_sent = true;
            $test_event_id = $this->send_test_event();
        }

        $enabled_for_php = !empty(WP_PAB_Tracker::get_instance()->get_token());

        $options = WP_PAB_Tracker::get_instance()->get_default_options();

        $uses_scoped_autoloader = defined('WP_PAB_SCOPED_AUTOLOADER') && WP_PAB_SCOPED_AUTOLOADER;

        ?>
        <div class="wrap">
            <h1>PAB WP</h1>

            <?php if ($test_event_sent): ?>
                <?php if ($test_event_id !== null): ?>
                    <div class="notice notice-success is-dismissible">
                        <p><?php echo translate("PHP test event sent successfully, with ID: <code>{$test_event_id}</code>!", 'pab-wp'); ?></p>
                    </div>
                <?php else: ?>
                    <div class="notice notice-error is-dismissible">
                        <p><?php esc_html_e('PHP failed to send test event. Check your configuration to make sure your token is set correctly.', 'pab-wp'); ?></p>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <table class="form-table" role="presentation">
                <tbody>
                <tr>
                    <th><?php esc_html_e('Enabled', 'pab-wp'); ?></th>
                    <td>
                        <fieldset>
                            <label for="pab-wp-php-enabled"
                                   title="<?php echo $uses_scoped_autoloader ? 'Using scoped vendor (plugin build)' : 'Using regular vendor (composer)'; ?>">
                                <input name="pab-wp-php-enabled" type="checkbox" id="pab-wp-php-enabled"
                                       value="0" <?php echo $enabled_for_php ? 'checked="checked"' : '' ?> readonly
                                       disabled>
                                <?php esc_html_e('PHP', 'pab-wp'); ?>
                            </label>
                        </fieldset>
                        <?php if (!$enabled_for_php): ?>
                            <p class="description">
                                <?php echo translate('To enable make sure <code>WP_PAB_TOKEN</code> contains a valid token.', 'pab-wp'); ?>
                            </p>
                            <br>
                        <?php endif; ?>
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="pab-wp-release"><?php esc_html_e('Release (version)', 'pab-wp'); ?></label>
                    </th>
                    <td>
                        <input type="text" class="regular-text code" readonly name="pab-wp-release"
                               value="<?php echo esc_html($options['release'] ?? ''); ?>"/>
                        <p class="description">
                            <?php echo translate('Change this value by defining <code>WP_PAB_VERSION</code>.', 'pab-wp'); ?>
                        </p>
                    </td>
                </tr>

                <tr>
                    <th>
                        <label for="pab-wp-environment"><?php esc_html_e('Environment', 'pab-wp'); ?></label>
                    </th>
                    <td>
                        <input type="text" class="regular-text code" readonly name="pab-wp-environment"
                               value="<?php echo esc_html($options['environment'] ?? ''); ?>"/>
                        <p class="description">
                            <?php echo translate('Change this value by defining <code>WP_PAB_ENV</code> or <code>WP_ENVIRONMENT_TYPE</code> (WordPress 5.5+).', 'pab-wp'); ?>
                        </p>
                    </td>
                </tr>

                <tr>
                    <th>
                        <label><?php esc_html_e('Test integration', 'pab-wp'); ?></label>
                    </th>
                    <td>
                        <form method="post">
                            <input type="submit" name="pab-wp-send-test-event-php" class="button"
                                   value="<?php esc_html_e('Send PHP test event', 'pab-wp') ?>" <?php echo $enabled_for_php ? '' : 'disabled'; ?>>
                        </form>
                        <?php if (!$enabled_for_php): ?>
                            <p class="description">
                                <?php echo translate('The tracker must be enabled to send a test event.', 'pab-wp'); ?>
                            </p>
                        <?php endif; ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    <?php }
}
