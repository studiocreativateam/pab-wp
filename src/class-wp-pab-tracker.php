<?php

use PAB\ClientBuilder;
use PAB\PABSdk;
use PAB\State\Hub;
use PAB\State\HubInterface;
use PAB\State\Scope;

final class WP_PAB_Tracker
{
    use WP_PAB_Resolve_User, WP_PAB_Resolve_Environment;

    /**
     * @var \PAB\ClientInterface
     */
    protected $client;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var self
     */
    private static $instance;

    public static function get_instance(): WP_PAB_Tracker
    {
        return self::$instance ?: self::$instance = new self;
    }

    protected function __construct()
    {
        if (defined('WP_PAB_SEND_DEFAULT_PII') && WP_PAB_SEND_DEFAULT_PII) {
            add_action('set_current_user', [$this, 'on_set_current_user']);
        }

        add_action('after_setup_theme', [$this, 'on_after_setup_theme']);

        $this->get_client();

        add_action('init', [$this, 'on_init']);
    }

    public function on_init(): void
    {
        if ($this->client === null) {
            return;
        }

        $hub = PABSdk::getCurrentHub();

        $hub->configureScope(function (Scope $scope) {
            foreach ($this->get_default_tags() as $tag => $value) {
                $scope->setTag($tag, $value);
            }
        });

        PABSdk::setCurrentHub($hub);
    }

    public function on_set_current_user(): void
    {
        $this->get_client()->configureScope(function (Scope $scope) {
            $scope->setUser($this->get_current_user_info());
        });
    }

    public function on_after_setup_theme(): void
    {
        if (has_filter('wp_pab_token')) {
            $this->initializeClient();
        }

        if (has_filter('wp_pab_scope')) {
            $this->get_client()->configureScope(function (Scope $scope) {
                apply_filters('wp_pab_scope', $scope);
            });
        }

        if (has_filter('wp_pab_options')) {
            apply_filters('wp_ab_options', $this->get_client()->getClient()->getOptions());
        }
    }

    public function get_token(): ?string
    {
        $token = defined('WP_PAB_PHP_TOKEN') ? WP_PAB_PHP_TOKEN : null;

        if ($token === null) {
            $token = defined('WP_PAB_TOKEN') ? WP_PAB_TOKEN : null;
        }

        if (has_filter('wp_pab_token')) {
            $token = (string)apply_filters('wp_pab_token', $token);
        }

        return $token;
    }

    public function get_private_key(): ?string
    {
        $privateKey = defined('WP_PAB_PRIVATE_KEY') ? WP_PAB_PRIVATE_KEY : null;

        if (has_filter('wp_pab_private_key')) {
            $privateKey = (string)apply_filters('wp_pab_private_key', $privateKey);
        }

        return $privateKey;
    }

    public function get_client(): HubInterface
    {
        if ($this->client === null && $this->get_token() !== null) {
            $this->initializeClient();
        }

        return PABSdk::getCurrentHub();
    }

    public function get_default_tags(): array
    {
        require WP_PAB_WPINC . '/version.php';

        $tags = [
            'wordpress' => $wp_version ?? 'unknown',
            'http.status_code' => http_response_code() ?: 0
        ];

        if (function_exists('get_bloginfo')) {
            $tags['language'] = get_bloginfo('language');
        }

        return $tags;
    }

    public function get_default_options(): array
    {
        $options = [
            'token' => $this->get_token(),
            'private_key' => $this->get_private_key(),
            'auth_data' => [
                'token' => $this->get_token(),
                'private_key' => $this->get_private_key(),
            ],
            'prefixes' => [ABSPATH],
            'environment' => $this->get_environment(),
            'send_default_pii' => defined('WP_PAB_SEND_DEFAULT_PII') && WP_PAB_SEND_DEFAULT_PII,
        ];

        if (defined('WP_PAB_VERSION')) {
            $options['release'] = WP_PAB_VERSION;
        }

        if (defined('WP_PAB_ERROR_TYPES')) {
            $options['error_types'] = WP_PAB_ERROR_TYPES;
        }

        $options['in_app_exclude'] = [
            WP_PAB_WPADMIN, // <base>/wp-admin
            WP_PAB_WPINC,   // <base>/wp-includes
        ];

        return $options;
    }

    private function initializeClient(): void
    {
        $token = $this->get_token();

        // Do not re-initialize the client when the TOKEN has not changed
        if ($this->client !== null && $this->token === $token) {
            return;
        }

        $this->token = $this->get_token();

        $clientBuilder = ClientBuilder::create($this->get_default_options());

        if (defined('WP_PAB_CLIENTBUILDER_CALLBACK') && is_callable(WP_PAB_CLIENTBUILDER_CALLBACK)) {
            call_user_func(WP_PAB_CLIENTBUILDER_CALLBACK, $clientBuilder);
        }

        $clientBuilder->setSdkIdentifier(WP_PAB_Version::SDK_IDENTIFIER);
        $clientBuilder->setSdkVersion(WP_PAB_Version::SDK_VERSION);

        $hub = new Hub($this->client = $clientBuilder->getClient());

        $hub->configureScope(function (Scope $scope) {
            foreach ($this->get_default_tags() as $tag => $value) {
                $scope->setTag($tag, $value);
            }
        });

        PABSdk::setCurrentHub($hub);
    }
}
