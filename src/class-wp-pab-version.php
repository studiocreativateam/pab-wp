<?php

final class WP_PAB_Version
{
    public const SDK_IDENTIFIER = 'pab.php.pab-wp-integration';
    public const SDK_VERSION = '0.1.1';
}
