<?php

trait WP_PAB_Resolve_Environment
{
    protected function get_environment(): string
    {
        $environment = defined('WP_PAB_ENV') ? WP_PAB_ENV : null;

        if ($environment === null && function_exists('wp_get_environment_type')) {
            $environment = wp_get_environment_type();
        }

        return $environment ?? 'unspecified';
    }
}
