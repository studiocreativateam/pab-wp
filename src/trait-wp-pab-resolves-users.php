<?php

trait WP_PAB_Resolve_User
{
    protected function get_current_user_info(): ?array
    {
        $current_user = wp_get_current_user();

        if ($current_user === null) {
            return null;
        }

        $user_context = $current_user instanceof WP_User && $current_user->exists() ? [
            'id' => $current_user->ID,
            'name' => $current_user->display_name,
            'email' => $current_user->user_email,
            'username' => $current_user->user_login,
        ] : [
            'id' => 0,
            'name' => 'anonymous',
        ];

        if (has_filter('wp_pab_user_context')) {
            return (array)apply_filters('wp_pab_user_context', $user_context);
        }

        return $user_context;
    }
}
